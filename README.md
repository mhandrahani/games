# Jeux de cartes

Jeu de cartes avec mélange aléatoire

## Installation 

- [Installation de Spring](https://docs.spring.io/spring-boot/docs/1.2.0.M2/reference/html/getting-started-installing-spring-boot.html) 
    
```    
:: Spring Boot ::                (v2.5.6)

$ java -version
openjdk version "1.8.0_292"
OpenJDK Runtime Environment (build 1.8.0_292-8u292-b10-0ubuntu1~20.04-b10)
OpenJDK 64-Bit Server VM (build 25.292-b10, mixed mode)
```
- Jnitialisation d'un projet avec [spring initializr](https://start.spring.io/)
```
Maven Projet
Java
Spring Boot 2.5.6
Packaging Jar
Java 8
Dependencies : ["Spring web", "Thymeleaf", "Spring Boot DevTools"]
```

- IDE : **IntelliJ IDEA 2018.3.4**


***

##  Utilisation : 

1 - Installation de nécessaire sur la machine listé ci-dessus.

2 - Clone de projet depuis gitlab : https://gitlab.com/mhandrahani/games

3 - Execution et puis test sur navigateur 

- [Exemple interface de Fcontioenement](https://gitlab.com/mhandrahani/games/-/blob/ceb601d4d01968f4ba22f403945dbfe158758afb/exempe-exec.png)


##  Évolution : 

- Mettre en place des tests de code 

- Meillieur architecture des Classes

- Developper le jeux Soliaire 

































