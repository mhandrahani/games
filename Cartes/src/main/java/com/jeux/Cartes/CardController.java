package com.jeux.Cartes;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class CardController {

    public static ArrayList<HashMap<String, String>> __list_random_card;

    @RequestMapping(value = { "/" })  // lancement page d'accueil
    public @ResponseBody ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }

    @GetMapping("/list-card/{nb_card}") // Recuperer 10 cartes de manière aleatoire
    public ArrayList<HashMap<String, String>> getCard(@PathVariable("nb_card") Integer nb_card) {

        String[] value_card = {"as", "2", "3", "4", "5", "6", "7", "8", "9", "10", "valet", "dame", "roi"};
        String[] color_card = {"trefle", "coeur", "pique", "carreau"};

        ArrayList<HashMap<String, String>> list_card = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < color_card.length; i++) {
            for (int j = 0; j < value_card.length; j++) {
                HashMap<String, String> card = new HashMap<>();
                card.put("color", color_card[i]);
                card.put("value", value_card[j]);
                list_card.add(card);
            }
        }
        ArrayList<HashMap<String, String>> list_random_card = new ArrayList<HashMap<String, String>>();
        for (int inc=0; inc < nb_card; inc++) {
            int rnd = new Random().nextInt(list_card.size());
            list_random_card.add(list_card.get(rnd));
            list_card.remove(rnd);
        }
        __list_random_card = list_random_card;
        System.out.println("Cartes non trié : " + list_random_card);
        return list_random_card;
    }


    @GetMapping("/sort-card") // Ordonner les cartes par couleur et valeur de maniere aleatoire
     public ArrayList<HashMap<String, ArrayList>> sortCard() {

        ArrayList<HashMap<String, ArrayList>> random_color_order = new ArrayList<HashMap<String, ArrayList>>();
        String[] color_card = new String[]{"trefle", "coeur", "pique", "carreau"};

        for (int i = 0; i < color_card.length; i++) {
            int index = (int) (Math.random() * color_card.length);
            String temp = color_card[i];
            color_card[i] = color_card[index];
            color_card[index] = temp;
        }

        for (int i = 0; i < color_card.length; i++) {
            HashMap<String, String> carte_order = new HashMap<>();
            for (int inc = 0; inc < __list_random_card.size(); inc++ ) {
                if (__list_random_card.get(inc).get("color") == color_card[i]) {
                    String color = color_card[i];
                    Boolean add_or_update = false;
                    Integer index = 0;
                    for (int j = 0; j < random_color_order.size(); j++ ) {
                        if (random_color_order.get(j).containsKey(color)) {
                            add_or_update = true;
                            index = j;
                        } else {
                            add_or_update = false;
                        }
                    }
                    if (add_or_update) {
                        random_color_order.get(index).get(color).add(__list_random_card.get(inc).get("value"));
                    } else {
                        HashMap<String, ArrayList> card_map = new HashMap<>();
                        ArrayList<String> arraylist = new ArrayList<String>();
                        arraylist.add(__list_random_card.get(inc).get("value"));
                        card_map.put(color, arraylist);
                        random_color_order.add(card_map);
                    }
                }
            }
        }

        for (int i = 0; i < random_color_order.size(); i++ ) {
            for (Map.Entry<String, ArrayList> entry : random_color_order.get(i).entrySet()) {
                String key = entry.getKey();
                ArrayList array_val = entry.getValue();
                Collections.shuffle(array_val);
            }
        }
        System.out.println("Cartes trié : " + random_color_order);
        return random_color_order;
    }
}